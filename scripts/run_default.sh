#!/bin/bash
# Launch navigation_arlobot on the command line
/usr/bin/pkill -f ros
source /opt/ros/kinetic/setup.bash
source ~/catkin_ws/devel/setup.bash
/usr/bin/python /opt/ros/kinetic/bin/roslaunch pozyx_drivers pozyx_ensadlab_nolidar.launch
