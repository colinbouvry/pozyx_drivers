#!/usr/bin/env python

import rospy
import pypozyx
import tf
import time

from pypozyx import *
from std_msgs.msg import Header
from geometry_msgs.msg import *
from sensor_msgs.msg import Imu
from pozyx_drivers.msg import AnchorInfo
from sensor_msgs.msg import FluidPressure
from pypozyx.definitions.bitmasks import POZYX_INT_MASK_IMU

doquit = False

class ReadyToLocalize(object):

    def __init__(self, pozyx, anchors, algorithm=POZYX_POS_ALG_TRACKING, dimension=POZYX_3D, height=1000, world_frame_ids=None, tag_frame_ids=None):
        self.pozyx = pozyx
        self.anchors = anchors
        self.algorithm = algorithm
        self.dimension = dimension
        self.height = height
        self.range_error_counts = [0 for i in xrange(len(self.anchors))]
        self.position_error_counts = [0 for i in xrange(len(tag_frame_ids))]
        self.sensor_error_counts = [0 for i in xrange(len(tag_frame_ids))]
        #self.error_23_counts = [0 for i in xrange(len(tag_frame_ids))]  
        #self.error_10_counts = [0 for i in xrange(len(tag_frame_ids))] 

        self.world_frame_ids = world_frame_ids
        self.tag_frame_ids = tag_frame_ids
        self.tag_error_code = [SingleRegister(0) for i in xrange(len(tag_frame_ids))]
        self.tag_connected = [False for i in xrange(len(tag_frame_ids))]
        self.anchor_connected = [True for i in xrange(len(anchors))]

        self.last_check_connected_time = time.time()
        self.anchor_i = 0
    def setup(self):
        """ets up the Pozyx for positioning by calibrating its anchor list"""
        #print("------------POZYX DRIVER ROS V{} -------------".format(version))
        print_("")
        print_(" - System will manually calibrate the tags")
        rospy.loginfo(" - System will manually calibrate the tags")
        print_("")
        print_(" - System will then auto start positioning")
        rospy.loginfo(" - System will then auto start positioning")
        print_("")
        
        uwb_settings = pypozyx.UWBSettings()
        self.pozyx.getUWBSettings(uwb_settings)

        tag_i = 0
        for tag_frame_id in self.tag_frame_ids:
            print_("")
            status_printdevice = self.pozyx.printDeviceInfo(tag_frame_id)
            #if tag_frame_id == None:
            #    self.pozyx.printDeviceInfo(tag_frame_id)
            #else:
            #   self.pozyx.printDeviceInfo(None)
           
            #print("------------POZYX DRIVER ROS V{} -------------".format(version))
            print_("")
            self.setAnchorsManual(tag_frame_id, save_to_flash=False)
            self.printPublishAnchorConfiguration()
            #self.printPublishConfigurationResult()
            
            #Filter
            self.pozyx.setPositioningFilterNone(tag_frame_id)
            
            if self.pozyx.checkUWBSettings(uwb_settings, tag_frame_id) is not True:
                s = "CheckUWBSettings problem : difference between None and " + str(tag_frame_id)
                print_(s)
                rospy.logwarn(s)

            print_("")
            print_("")
            tag_i = tag_i + 1
                
        #check connection anchors
        for anchor_i in range(len(self.anchors)):
            anchor_id = self.anchors[anchor_i].network_id
            self.checkConnectionAnchors(anchor_id, anchor_i)

        #check 2 minimum anchor connected
        anchor_connected_count = 0
        for a in range(len(self.anchor_connected)):
            if self.anchor_connected[a] == True:
                anchor_connected_count += 1
        if anchor_connected_count < 3:
            s = "Anchors connected < 3 :"
            rospy.logerr(s)
            print_(s)
            self.onQuit()
        else:
            #check tags
            tag_i = 0
            for tag_frame_id in self.tag_frame_ids:
                self.checkConnectionTags(tag_frame_id, tag_i)
                tag_i = tag_i + 1

    def loop(self):
        #time event
        do_tentative_reconnect = False
        now = time.time()
        if now - self.last_check_connected_time > 5:
            self.last_check_connected_time = now
            do_tentative_reconnect = True

        #loop tag
        tag_i = 0
        for tag_frame_id in self.tag_frame_ids:
            
            if True: #tag_frame_id is not None: #or self.pozyx.checkForFlag(POZYX_INT_MASK_IMU, 0.01) == POZYX_SUCCESS:
                
                world_frame_id = self.world_frame_ids[tag_i]

                if self.tag_connected[tag_i] == False:
                    if do_tentative_reconnect == True:
                        #self.checkConnection(tag_frame_id, tag_i)
                        bconnect = self.checkConnectionTagsCount(tag_frame_id, tag_i)
                        rospy.loginfo("Connect " + str(bconnect) + " tag " + hex(tag_frame_id))
                        if bconnect == False:
                            continue
                    else:
                        continue
                            
                #Topic : PoseWitchCovariance
                pwc = PoseWithCovarianceStamped()
                pwc.header.stamp = rospy.get_rostime()
                pwc.header.frame_id = world_frame_id
                pwc.pose.pose.position = Coordinates()
                pwc.pose.pose.orientation =  pypozyx.Quaternion()
                cov = pypozyx.PositionError()

                #Positioning
                status_pos = self.pozyx.doPositioning(pwc.pose.pose.position, self.dimension, self.height, self.algorithm, tag_frame_id)
                if status_pos == POZYX_SUCCESS:
                    #Convert from mm to m
                    pwc.pose.pose.position.x = pwc.pose.pose.position.x * 0.001
                    pwc.pose.pose.position.y = pwc.pose.pose.position.y * 0.001
                    pwc.pose.pose.position.z = pwc.pose.pose.position.z * 0.001
                    self.printPublishPosition(pwc.pose.pose.position, tag_frame_id)
                    #self.strategyErrorCount(SingleRegister(0), tag_frame_id, tag_i)
                    self.strategyPositionErrorCount(SingleRegister(0), tag_frame_id, tag_i)
                else:
                    error_code = SingleRegister(23)
                    self.updateErrorCode(error_code, "positioning", tag_frame_id)
                    #self.strategyErrorCount(error_code, tag_frame_id, tag_i)
                    self.strategyPositionErrorCount(error_code, tag_frame_id, tag_i)

                #Gets new IMU sensor data"""
                sensor_data = SensorData() 
                status_sensor = self.pozyx.getAllSensorData(sensor_data, tag_frame_id)
                if status_sensor == POZYX_SUCCESS:
                    pwc.pose.pose.orientation = sensor_data.quaternion
                    #self.strategyErrorCount(SingleRegister(0), tag_frame_id, tag_i)
                    self.strategySensorErrorCount(SingleRegister(0), tag_frame_id, tag_i)
                else:
                    error_code = SingleRegister(23)
                    self.updateErrorCode(error_code, "all sensor data", tag_frame_id)
                    #self.strategyErrorCount(error_code, tag_frame_id, tag_i)
                    self.strategySensorErrorCount(error_code, tag_frame_id, tag_i)

                #Quaternion    
                #status_quat = self.pozyx.getQuaternion(pwc.pose.pose.orientation, tag_frame_id)
                #if status_quat != POZYX_SUCCESS:
                #    self.printPublishErrorCode("quaternion", tag_frame_id)
                #    strategyError()
             
                #Position covariance    
                if status_pos == POZYX_SUCCESS:
                    status_cov = self.pozyx.getPositionError(cov, tag_frame_id)
                    if status_cov == POZYX_SUCCESS:
                        cov_row1 =[cov.x, cov.xy, cov.xz, 0, 0, 0]
                        cov_row2 =[cov.xy, cov.y, cov.yz, 0, 0, 0]
                        cov_row3 =[cov.xz, cov.yz, cov.z, 0, 0, 0]
                        cov_row4 =[0, 0, 0, 0, 0, 0]
                        cov_row5 =[0, 0, 0, 0, 0, 0]
                        cov_row6 =[0, 0, 0, 0, 0, 0]

                        pwc.pose.covariance = cov_row1 + cov_row2 + cov_row3 + cov_row4 + cov_row5 + cov_row6    
                    else:
                        error_code = SingleRegister()
                        self.updateErrorCode(error_code, "position error", tag_frame_id)

                #Calibration status
                if status_sensor == POZYX_SUCCESS:
                    calibration_status = SingleRegister()
                    status_calib = self.pozyx.getCalibrationStatus(calibration_status, tag_frame_id)
                    if status_calib == POZYX_SUCCESS:
                        #Adds the calibration status data to the log
                        s = ""
                        if tag_frame_id is not None:
                            s = "Calibration status {} : ".format("0x%0.4x" % tag_frame_id)
                        else:
                            s = "Calibration status None :"
                        s += bin(calibration_status[0])
                        #s += " : " + str(calibration_status[0] & 0x03)
                        #s += ", " + str((calibration_status[0] & 0x0C) >> 2)
                        #s += ", " + str((calibration_status[0] & 0x30) >> 4)
                        #s += ", " + str((calibration_status[0] & 0xC0) >> 6)
                        rospy.logdebug(s)
                        print_(s)
                    else:
                        error_code = SingleRegister()
                        self.updateErrorCode(error_code, "calibration error", tag_frame_id)


                #Euler angles 
                #angles = pypozyx.EulerAngles()
                #status_euler = self.pozyx.getEulerAngles_deg(angles, tag_frame_id) 
                #if status_euler == POZYX_SUCCESS:
                #    self.printPublishAngles(angles, tag_frame_id)
                #else:
                #"    self.printPublishErrorCode("Euler angles", tag_frame_id)

                #Topic : IMU
                #imu = Imu()
                #imu.header.stamp = rospy.get_rostime()
                #imu.header.frame_id = world_frame_id
                #imu.orientation =  pypozyx.Quaternion()
                #imu.orientation_covariance = [0,0,0,0,0,0,0,0,0]
                #imu.angular_velocity = pypozyx.AngularVelocity()
                #imu.angular_velocity_covariance = [0,0,0,0,0,0,0,0,0]
                #imu.linear_acceleration = pypozyx.LinearAcceleration()
                #imu.linear_acceleration_covariance = [0,0,0,0,0,0,0,0,0]

                #self.pozyx.getQuaternion(imu.orientation, tag_frame_id)
                #self.pozyx.getAngularVelocity_dps(imu.angular_velocity, tag_frame_id)
                #self.pozyx.getLinearAcceleration_mg(imu.linear_acceleration, tag_frame_id)

                #Convert from mg to m/s2
                #imu.linear_acceleration.x = imu.linear_acceleration.x * 0.0098
                #imu.linear_acceleration.y = imu.linear_acceleration.y * 0.0098
                #imu.linear_acceleration.z = imu.linear_acceleration.z * 0.0098

                #Convert from Degree/second to rad/s
                #imu.angular_velocity.x = imu.angular_velocity.x * 0.01745
                #imu.angular_velocity.y = imu.angular_velocity.y * 0.01745
                #imu.angular_velocity.z = imu.angular_velocity.z * 0.01745

                #pub_imu[tag_i].publish(imu)

                #Topic : PoseStamped and Pose with covariance
                if status_pos == POZYX_SUCCESS and status_sensor == POZYX_SUCCESS:
                    ps = PoseStamped()
                    ps.header.stamp = rospy.get_rostime()
                    ps.header.frame_id = world_frame_id
                    ps.pose.position = pwc.pose.pose.position
                    ps.pose.orientation =  pwc.pose.pose.orientation
                    pub_pose[tag_i].publish(ps)
                    
                    if status_cov == POZYX_SUCCESS:
                        pub_pose_with_cov[tag_i].publish(pwc)    
                    
                #Topic : Pressure
                #pr = FluidPressure()
                #pr.header.stamp = rospy.get_rostime()
                #pr.header.frame_id = world_frame_id
                #pressure = pypozyx.Pressure()
            
                #self.pozyx.getPressure_Pa(pressure, tag_frame_id)
                #pr.fluid_pressure = pressure.value
                #pr.variance = 0

                #pub_pressure[tag_i].publish(pr)
            tag_i+=1
            
        #Topic : Anchors Info
        #for i in range(len(self.anchors)):
        #self.updateAnchor(self.anchor_i)
        self.anchor_i = self.anchor_i + 1
        self.anchor_i = self.anchor_i % (len(anchors))
        anchor_id = self.anchors[self.anchor_i].network_id

        if self.anchor_connected[self.anchor_i] == False:
            if do_tentative_reconnect == True:
                #self.checkConnectionAnchors(anchor_id, self.anchor_i) 
                bconnect = self.checkConnectionAnchorsCount(anchor_id, self.anchor_i)
                rospy.loginfo("Connect " + str(bconnect) + " anchor " + hex(anchor_id))
                if bconnect == False:
                    return
            else:
                return

        self.updateAnchor(self.anchor_i)

    def onTagDisconnect(self, network_id, i):
        if network_id is None:
            s = "EVENT onTagDisconnect None :"
        else:
            s = "EVENT onTagDisconnect {} : ".format("0x%0.4x" % network_id)
        rospy.loginfo(s)
        print_(s)
        self.tag_connected[i] = False
        pub_onTagDisconnect.publish(data="tag_" + str(i+1))

    def onTagConnect(self, network_id, i):
        if network_id is None:
            s = "EVENT onTagConnect None :"
        else:
            s = "EVENT onTagConnect {} : ".format("0x%0.4x" % network_id)
        rospy.loginfo(s)
        print_(s)
        self.tag_connected[i] = True
        pub_onTagConnect.publish(data="tag_" + str(i+1))
 
    def onAnchorConnect(self, network_id, i):
        s = "EVENT onAnchorConnect " + hex(network_id) + " :"
        rospy.loginfo(s)
        print_(s)
        self.anchor_connected[i] = True
        #pub_onTagConnect.publish(data="anchor_" + str(i+1))

    def onAnchorDisconnect(self, network_id, i):
        s = "EVENT onAnchorDisconnect "  + hex(network_id) + " :"
        rospy.loginfo(s)
        print_(s)
        self.anchor_connected[i] = False
        #pub_onTagDisconnect.publish(data="anchor_" + str(i+1))

    def onQuit(self):
        i = 0
        for tag_frame_id in self.tag_frame_ids:
            if self.tag_connected[i] == True:
                self.onTagDisconnect(tag_frame_id, i)
            i = i + 1

        global doquit
        doquit = True
        rospy.loginfo("DOQUIT")
        print_("DOQUIT")

    def strategyError_(self, error_code, network_id, i):
        #print_("error_code " + str(error_code.value))
        if self.tag_error_code[i].value != error_code.value:
            if error_code.value == 23:
                s = "Error code 23 managed !"
                rospy.logerr(s)
                print_(s)
                self.onTagDisconnect(network_id, i)
                if network_id is None:
                    self.onQuit()
            elif self.tag_error_code[i].value == 23:
                s = "Error code 23 changed and managed !"
                rospy.logerr(s)
                print_(s)
                self.onQuit()
        self.tag_error_code[i] = error_code

    def strategyErrorCount(self, error_code, network_id, i):
        #print_("error_code " + str(error_code.value))
        if error_code.value == 23:
            self.error_23_counts[i] += 1

            if self.error_23_counts[i] == 9:
                s = "Error count code 23 managed !"
                rospy.logerr(s)
                print_(s)
                self.onTagDisconnect(network_id, i)
                if network_id is None:
                    self.onQuit()
        else:
            if self.error_23_counts[i] > 9:
                s = "Error count code 23 finished and managed !"
                rospy.logerr(s)
                print_(s)
                self.onQuit()
            self.error_23_counts[i] = 0

    def strategySensorErrorCount(self, error_code, network_id, i):
        #print_("error_code " + str(error_code.value))
        if error_code.value != 0:
            self.sensor_error_counts[i] += 1

            if self.sensor_error_counts[i] == 9:
                s = "Sensor error count managed !"
                rospy.logerr(s)
                print_(s)
                if self.tag_connected[i] == True:
                    self.onTagDisconnect(network_id, i)
                    if network_id is None:
                        self.onQuit()
        else:
            if self.sensor_error_counts[i] > 9:
                s = "Sensor error count finished and managed !"
                rospy.logerr(s)
                print_(s)
                self.onQuit()
            self.sensor_error_counts[i] = 0

    def strategyPositionErrorCount(self, error_code, network_id, i):
        #print_("error_code " + str(error_code.value))
        if error_code.value != 0:
            self.position_error_counts[i] += 1

            if self.position_error_counts[i] == 9:
                s = "Position error count managed !"
                rospy.logerr(s)
                print_(s)
                if self.tag_connected[i] == True:
                    self.onTagDisconnect(network_id, i)
                    if network_id is None:
                        self.onQuit()
        else:
            if self.position_error_counts[i] > 9:
                s = "Position error count finished and managed !"
                rospy.logerr(s)
                print_(s)
                self.onQuit()
            self.position_error_counts[i] = 0

    def strategyRangeErrorCount(self, error_code, network_id, i):
        #print_("error_code " + str(error_code.value))
        if error_code.value != 0:
            self.range_error_counts[i] += 1

            if self.range_error_counts[i] == 9:
                s = "Range error count managed at id " + str(network_id)
                rospy.logerr(s)
                print_(s)
                self.onAnchorDisconnect(network_id, i)

                anchor_connected_count = 0                
                for a in range(len(self.anchor_connected)):
                    if self.anchor_connected[a] == True:
                        anchor_connected_count += 1
                if anchor_connected_count < 3:
                    s = "Anchors connected < 3 :"
                    rospy.logerr(s)
                    print_(s)
                    self.onQuit()
        else:
            if self.range_error_counts[i] > 9:
                s = "Range error count finished and managed at id " + str(network_id)
                rospy.logerr(s)
                print_(s)
                self.onAnchorConnect(network_id, i)
            self.range_error_counts[i] = 0 

    def setAnchorsManual(self, network_id = None, save_to_flash=False):
        status = self.pozyx.clearDevices(network_id)
        for anchor in self.anchors:
            status &= self.pozyx.addDevice(anchor, network_id)
        if len(anchors) > 4:
            status &= self.pozyx.setSelectionOfAnchors(POZYX_ANCHOR_SEL_AUTO, len(anchors), network_id)

        if save_to_flash:
            self.pozyx.saveAnchorIds(network_id)
            self.pozyx.saveRegisters([PozyxRegisters.POSITIONING_NUMBER_OF_ANCHORS], network_id)

        return status

    def printPublishPosition(self, position, network_id):
        """Prints the Pozyx's position"""
        if network_id is None:
            network_id = 0
        s = "ID: {}, x(m): {}, y(m): {}, z(m): {}".format("0x%0.4x" % network_id, position.x, position.y, position.z)
        rospy.logdebug(s)
        print_(s)
    
    def printPublishAngles(self, angles, network_id):
        """Prints the Pozyx's angle"""
        if network_id is None:
            network_id = 0
        s = "ID: {}, r: {}, p: {}, y: {}".format("0x%0.4x" % network_id, angles[0], angles[1], angles[2])
        rospy.logdebug(s)
        print_(s)
        
    def printPublishAnchorConfiguration(self):
        for anchor in self.anchors:
            s = "ANCHOR,0x%0.4x,%s" % (anchor.network_id, str(anchor.pos))
            rospy.loginfo(s)
            print_(s)
                    
    def printPublishConfigurationResult(self):
        list_size = SingleRegister()
        status = self.pozyx.getDeviceListSize(list_size)
        device_list = DeviceList(list_size=list_size[0])
        status = self.pozyx.getDeviceIds(device_list)

        s = "Anchors configuration:" + "Anchors found: {0}".format(list_size[0])
        rospy.loginfo(s)
        print_(s)

        for i in range(list_size[0]):
            anchor_coordinates = Coordinates()
            status = self.pozyx.getDeviceCoordinates(device_list[i], anchor_coordinates)
            s = ("ANCHOR,0x%0.4x, %s" % (device_list[i], str(anchor_coordinates)))
            rospy.loginfo(s)
            print_(s)
            
    def updateErrorCode(self, error_code, operation, network_id):
        """Prints the Pozyx's error and possibly sends it as a OSC packet"""
        if network_id is None:
            self.pozyx.getErrorCode(error_code)
            s = "LOCAL ERROR %s, %s" % (operation, self.pozyx.getErrorMessage(error_code))
            rospy.logwarn(s)
            print_(s)
            return
        status = self.pozyx.getErrorCode(error_code, network_id)
        if status == POZYX_SUCCESS:
            s = ("ERROR %s on ID %s, %s" % (operation, "0x%0.4x" % network_id, self.pozyx.getErrorMessage(error_code)))
            rospy.logwarn(s)
            print_(s)
        else:
            self.pozyx.getErrorCode(error_code)
            s = "ERROR %s, couldn't retrieve remote error code, LOCAL ERROR %s" % (operation, self.pozyx.getErrorMessage(error_code))
            rospy.logwarn(s)
            print_(s)

    def checkConnectionTags(self, network_id, i):
        """Check connection"""
        status_discovery = self.pozyx.doDiscoveryTags(2, 0.005, network_id)
        if status_discovery == POZYX_SUCCESS: 
            self.onTagConnect(network_id, i)
        else:
            self.onTagDisconnect(network_id, i)

    def checkConnectionTagsCount(self, network_id, i):
        """Check connection"""
        status_discovery = self.pozyx.doDiscoveryTags(2, 0.005, network_id)
        if status_discovery == POZYX_SUCCESS: 
            self.position_error_counts[i] = 10
            self.sensor_error_counts[i] = 10
            return True
        else:
            self.position_error_counts[i] = 0
            self.sensor_error_counts[i] = 0
            return False

    def checkConnectionAnchors(self, network_id, i):
        """Check connection"""
        status_discovery = self.pozyx.doDiscoveryAnchors(2, 0.005, network_id)
        if status_discovery == POZYX_SUCCESS: 
            self.onAnchorConnect(network_id, i)
        else:
            self.onAnchorDisconnect(network_id, i)

    def checkConnectionAnchorsCount(self, network_id, i):
        """Check connection"""
        status_discovery = self.pozyx.doDiscoveryAnchors(2, 0.005, network_id)
        if status_discovery == POZYX_SUCCESS: 
            self.range_error_counts[i] = 10
            return True
        else:
            self.range_error_counts[i] = 0
            return False

    def checkStrategyErrorCount_(self, network_id, i):
        """Check connection"""
        status_discovery = self.pozyx.doDiscoveryTags(2, 0.005, network_id)
        if status_discovery == POZYX_SUCCESS: 
            self.strategyErrorCount(SingleRegister(0), network_id, i)
            self.error_23_counts[i] = 0
        else:
            self.strategyErrorCount(SingleRegister(23), network_id, i)
            self.error_23_counts[i] = 10

    def checkStrategyErrorCount_(self, network_id, i):
        """Check connection"""
        rospy.loginfo("checkStrategyErrorCount")
        status_discovery = self.pozyx.doDiscoveryTags(2, 0.005, network_id)
        if status_discovery == POZYX_SUCCESS: 
            #self.position_error_counts[i] = 0
            #self.sensor_error_counts[i] = 0
            self.strategyPositionErrorCount(SingleRegister(0), network_id, i)
            #self.strategySensorErrorCount(SingleRegister(0), network_id, i)
        else:
            #self.position_error_counts[i] = 10
            #self.sensor_error_counts[i] = 10
            self.strategyPositionErrorCount(SingleRegister(23), network_id, i)
            #self.strategySensorErrorCount(SingleRegister(23), network_id, i)

    def checkStrategyError_(self, network_id, i):
        """Check connetion"""
        status_discovery = self.pozyx.doDiscoveryTags(2, 0.005, network_id)
        if status_discovery == POZYX_SUCCESS: 
            self.strategyError(SingleRegister(0), network_id, i)
        else:
            self.strategyError(SingleRegister(23), network_id, i)

    def updateAnchor(self, i):
        """Update anchor and pub"""
        dr = AnchorInfo()
        dr.header.stamp = rospy.get_rostime()
        dr.header.frame_id = "anchor"
        anchor_id = self.anchors[i].network_id
        dr.id = hex(anchor_id)
        dr.position.x = (float)(self.anchors[i].pos.x) * 0.001
        dr.position.y = (float)(self.anchors[i].pos.y) * 0.001
        dr.position.z = (float)(self.anchors[i].pos.z) * 0.001

        device_range = DeviceRange()
        status = self.pozyx.doRanging(self.anchors[i].network_id, device_range)
        dr.distance = (float)(device_range.distance) * 0.001
        dr.RSS = device_range.RSS

        if status == POZYX_SUCCESS:
            dr.status = True
            #self.strategyErrorCount(SingleRegister(0), tag_frame_id, tag_i)
            self.strategyRangeErrorCount(SingleRegister(0), anchor_id, i)
        else:
            dr.status = False
            error_code = SingleRegister(23)
            self.updateErrorCode(error_code, "ranging", anchor_id)
            #self.strategyErrorCount(error_code, tag_frame_id, tag_i)
            self.strategyRangeErrorCount(error_code, anchor_id, i)


        #if status == POZYX_SUCCESS:
        #    dr.status = True
        #    self.range_error_counts[i] = 0
        #else:
        #    dr.status = False
        #    self.range_error_counts[i] += 1
        #    if self.range_error_counts[i] > 9:
        #        self.range_error_counts[i] = 0
        #        s = "Anchor " + str(i) + " (" + str(dr.id) + ") lost"
        #        rospy.logerr(s)
        #        print_(s)

        # pub_anchors_info.publish(dr)
        if i == 0:
            dr.child_frame_id = "anchor_0"
            pub_anchor1_info.publish(dr)
        elif i == 1:
            dr.child_frame_id = "anchor_1"
            pub_anchor2_info.publish(dr)
        elif i == 2:
            dr.child_frame_id = "anchor_2"
            pub_anchor3_info.publish(dr)
        elif i == 3:
            dr.child_frame_id = "anchor_3"
            pub_anchor4_info.publish(dr)

def print_(s):
    if verbose:
        print(s)

if __name__ == "__main__":
    rospy.init_node('pozyx_node', log_level=rospy.INFO)

    # Reading parameters
    verbose = rospy.get_param('~verbose', False)

    print_all_serial_ports()
    serial_first = get_first_pozyx_serial_port()
    print_("First pozyx serial port : ")
    print_( serial_first)
    rospy.loginfo("First pozyx serial port : ")
    rospy.loginfo(serial_first)

    # serial_port = get_serial_ports()[0].device
    serial_port = rospy.get_param('~serial_port')
    
    anchor0_id = int(rospy.get_param('~anchor0_id'), 16)
    anchor1_id = int(rospy.get_param('~anchor1_id'), 16)
    anchor2_id = int(rospy.get_param('~anchor2_id'), 16)
    anchor3_id = int(rospy.get_param('~anchor3_id'), 16)

    anchor0_coordinates = eval(rospy.get_param('~anchor0_coordinates'))
    anchor1_coordinates = eval(rospy.get_param('~anchor1_coordinates'))
    anchor2_coordinates = eval(rospy.get_param('~anchor2_coordinates'))
    anchor3_coordinates = eval(rospy.get_param('~anchor3_coordinates'))

    algorithm = int(rospy.get_param('~algorithm'))
    dimension = int(rospy.get_param('~dimension'))
    height    = int(rospy.get_param('~height'))
    frequency = int(rospy.get_param('~frequency'))

    world_frame_ids = rospy.get_param('~world_frame_id').split("; ")
    tag_frame_ids_ = rospy.get_param('~tag_frame_id').split("; ")
    tag_frame_ids = []
    
    s = "world_frame_ids".join(world_frame_ids)
    rospy.loginfo(s)
    print_(s)
    
    s = "tag_frame_ids_".join(tag_frame_ids_)
    rospy.loginfo(s)
    print_(s)
    
    for tag_frame_id_ in tag_frame_ids_:
        tag_id = int(tag_frame_id_, 16)
        if tag_id != 0x00:
            tag_frame_ids.append(tag_id)
        else:
            tag_frame_ids.append(None)
    
    if tag_frame_ids[0] != None:    
        s = "tag_frame_ids".join(tag_frame_ids)
    rospy.loginfo(s)
    print_(s)
        
    # tag_frame_id = int(rospy.get_param('~tag_frame_id'), 16)
    
    size_ = len(tag_frame_ids)
    if size_ == 0:
        tag_frame_ids.append(None)
    if size_ == 1:
        tag_frame_ids[0] = None

    # Creating publishers
    pub_anchor1_info = rospy.Publisher('~anchor_info_0', AnchorInfo, queue_size=1)
    pub_anchor2_info = rospy.Publisher('~anchor_info_1', AnchorInfo, queue_size=1)
    pub_anchor3_info = rospy.Publisher('~anchor_info_2', AnchorInfo, queue_size=1)
    pub_anchor4_info = rospy.Publisher('~anchor_info_3', AnchorInfo, queue_size=1)
    
    
    pub_pose_with_cov = []
    pub_imu = []
    pub_pose = []
    pub_pressure = []
    
    for num in range(size_):
        suffixe = ''
        if num > 0: 
            suffixe = str(num+1)
        pub_pose_with_cov.append(rospy.Publisher('~pose_with_cov' + suffixe, PoseWithCovarianceStamped, queue_size=1))
        pub_imu.append(rospy.Publisher('~imu' + suffixe, Imu, queue_size=1))
        pub_pose.append(rospy.Publisher('~pose' + suffixe, PoseStamped , queue_size=1))
        pub_pressure.append(rospy.Publisher('~pressure' + suffixe, FluidPressure , queue_size=1))
    
    pub_onTagDisconnect = rospy.Publisher('~onTagDisconnect', std_msgs.msg.String, queue_size=1)
    pub_onTagConnect = rospy.Publisher('~onTagConnect', std_msgs.msg.String, queue_size=1)
 
    # set anchors
    anchors = [DeviceCoordinates(anchor0_id, 1, Coordinates(anchor0_coordinates[0], anchor0_coordinates[1], anchor0_coordinates[2])),
               DeviceCoordinates(anchor1_id, 1, Coordinates(anchor1_coordinates[0], anchor1_coordinates[1], anchor1_coordinates[2])),
               DeviceCoordinates(anchor2_id, 1, Coordinates(anchor2_coordinates[0], anchor2_coordinates[1], anchor2_coordinates[2])),
               DeviceCoordinates(anchor3_id, 1, Coordinates(anchor3_coordinates[0], anchor3_coordinates[1], anchor3_coordinates[2]))]

    rate = rospy.Rate(frequency)

    # Starting communication with Pozyx
    if serial_port == "":
        serial_port = serial_first

    try:
        pozyx = PozyxSerial(serial_port, baudrate=115200, timeout=0.1, write_timeout=0.1, print_output=False, debug_trace=False, show_trace=False, suppress_warnings=False)
    except:
        doquit = True
        rospy.logerr("Exception PozyxSerial")
        print_("Exception PozyxSerial")

    if doquit == False:
        rdl = ReadyToLocalize(pozyx, anchors, algorithm, dimension, height, world_frame_ids, tag_frame_ids)
        rdl.setup()

    while doquit == False and not rospy.is_shutdown():
        rdl.loop()
        #try:
        #   rdl.loop()
        #except:
        #    doquit = True
        #    rospy.logerr("Exception loop")
        #    print_("Exception loop")
        rate.sleep()
    print_("QUIT")
    rospy.loginfo("QUIT")
